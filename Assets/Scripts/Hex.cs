﻿using System.Collections.Generic;
using UnityEngine;

public class Hex {
    protected HexGrid hexGrid;

    public int q;// Cube Coordinates
    public int r;// Cube Coordinates
    public Vector2 pos;// Pixel coordinates
    public Dictionary<HexGrid.GD, Hex> neighbors;
    public int fieldValue;
    public Hex nextInPath;

    private GameObject wallGO;

    public Vector3[] vertexCoords;

    public Hex(HexGrid hexGrid, int q, int r) {
        this.hexGrid = hexGrid;
        this.q = q;
        this.r = r;
        pos = hexGrid.HexToPixel(this);

        neighbors = new Dictionary<HexGrid.GD, Hex>();
        wallGO = null;

        vertexCoords = GetVertexCoordinates();
    }

    public void AddNeighbor(HexGrid.GD d, Hex h) {
        if (!neighbors.ContainsKey(d)) {
            neighbors.Add(d, h);
        }
    }

    public Hex GetNeighborAt(HexGrid.GD d) {
        return neighbors[d];
    }

    public void ToggleWall() {
        if (wallGO == null) {
            wallGO = GameObject.Instantiate(GameController.instance.wallPrefab);
            wallGO.transform.position = pos;
        } else {
            GameObject.Destroy(wallGO);
            wallGO = null;
        }
    }

    // Cost of passing through the hex cell
    public int GetCost() {
        if (wallGO == null) {
            return 2;
        } else {
            return 1000;
        }
    }

    /*
     * Returns the corners of the hexagon with top corner duplicated (for drawing)
     */
    private Vector3[] GetVertexCoordinates() {
        Vector3[] res = new Vector3[7];

        res[0] = new Vector2(
            pos.x, pos.y - HexGrid.hexOuterRadius);
        res[1] = new Vector2(
            pos.x + HexGrid.hexInnerRadius, pos.y - HexGrid.hexOuterRadius / 2);
        res[2] = new Vector2(
            pos.x + HexGrid.hexInnerRadius, pos.y + HexGrid.hexOuterRadius / 2);
        res[3] = new Vector2(
            pos.x, pos.y + HexGrid.hexOuterRadius);
        res[4] = new Vector2(
            pos.x - HexGrid.hexInnerRadius, pos.y + HexGrid.hexOuterRadius / 2);
        res[5] = new Vector2(
            pos.x - HexGrid.hexInnerRadius, pos.y - HexGrid.hexOuterRadius / 2);
        res[6] = new Vector2(
            pos.x, pos.y - HexGrid.hexOuterRadius);

        return res;
    }
}