﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public Camera mainCamera;
    public HexGrid grid;

    public GameObject wallPrefab;
    public GameObject actorPrefab;

    public Transform[] actorSpawnPositions;
    public float actorsPerSecond;

    public void Awake() {
        instance = this;
    }

    public void Start() {
        StartCoroutine(SpawnActorsProcess());
    }

    public void Update() {
        if (Input.GetMouseButtonDown(0)) {
            grid.ToggleWallOnHex(mainCamera.ScreenToWorldPoint(Input.mousePosition));
        }
    }

    private IEnumerator SpawnActorsProcess() {
        float leftOverActorsToSpawn = 0f;
        Transform actor;
        while (true) {
            leftOverActorsToSpawn += actorsPerSecond * Time.deltaTime;
            for (int i = 0; i < Mathf.FloorToInt(leftOverActorsToSpawn); i++) {
                for (int j = 0; j < actorSpawnPositions.Length; j++) {
                    actor = Instantiate(actorPrefab).GetComponent<Transform>();
                    actor.position = actorSpawnPositions[j].position;
                }
            }
            leftOverActorsToSpawn %= 1;
            yield return null;
        }
    }
}