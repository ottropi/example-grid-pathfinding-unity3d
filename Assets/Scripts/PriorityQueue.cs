﻿using UnityEngine;
using System.Collections.Generic;

/*
 * Enqueues keys with any priority.
 * Dequeues the lowest-priority key. If more than 1 key has the same
 * lowest priority, a random key will be dequeued.
 * The priority is a float.
 * Uses a list to store the binary tree.
 */
public class PriorityQueue<TKey> : object {

    private List<PriorityQueueItem<TKey>> items;
    private HashSet<TKey> allKeys;

    private PriorityQueueItem<TKey> tempItem;

    public PriorityQueue() {
        items = new List<PriorityQueueItem<TKey>>();
        allKeys = new HashSet<TKey>();
    }

    public void Clear() {
        items.Clear();
        allKeys.Clear();
    }

    /*
     * Adds the element at the bottom of the tree and swaps it
     * with its parent until the parent has a lower priority.
     */
    public void Enqueue(TKey key, float priority) {
        PriorityQueueItem<TKey> item = new PriorityQueueItem<TKey>(key, priority);
        items.Add(item);
        allKeys.Add(key);

        int newItemIndex = items.Count - 1;
        int parentIndex = Mathf.FloorToInt((float)(newItemIndex - 1) / 2);

        while (parentIndex >= 0 && items[newItemIndex].priority < items[parentIndex].priority) {
            Swap(parentIndex, newItemIndex);
            newItemIndex = parentIndex;
            parentIndex = Mathf.FloorToInt((float)(newItemIndex - 1) / 2);
        }
    }

    /*
     * Returns the first element of the tree and removes it.
     * Moves the last item of the tree to the first position
     * and swaps it with its childs until both children have
     * a higher priority.
     */
    public TKey Dequeue() {
        TKey res = default(TKey);

        if (items.Count > 0) {
            res = items[0].key;
            allKeys.Remove(res);

            // Replaces the first with the last
            items[0] = items[items.Count - 1];

            // Deletes the last
            items.RemoveAt(items.Count - 1);

            int parentIndex = 0, leftChildIndex, rightChildIndex;
            float parentPriority, leftChildPriority, rightChildPriority;
            bool finished = false;

            // Compares the parent with its childs. If the child has a lower priority,
            // both are swapped
            while (parentIndex < items.Count && !finished) {

                leftChildIndex = 2 * parentIndex + 1;
                rightChildIndex = 2 * parentIndex + 2;

                if (leftChildIndex >= items.Count) {
                    leftChildIndex = parentIndex;
                }

                if (rightChildIndex >= items.Count) {
                    rightChildIndex = parentIndex;
                }

                finished = true;

                parentPriority = items[parentIndex].priority;
                leftChildPriority = items[leftChildIndex].priority;
                rightChildPriority = items[rightChildIndex].priority;

                if (leftChildPriority < parentPriority ||
                    rightChildPriority < parentPriority) {
                    if (leftChildPriority < rightChildPriority) {
                        Swap(leftChildIndex, parentIndex);
                        parentIndex = leftChildIndex;
                        finished = false;
                    } else {
                        Swap(rightChildIndex, parentIndex);
                        parentIndex = rightChildIndex;
                        finished = false;
                    }
                }
            }
        }
        return res;
    }

    public bool Contains(TKey key) {
        return allKeys.Contains(key);
    }

    private void Swap(int a, int b) {
        tempItem = items[a];
        items[a] = items[b];
        items[b] = tempItem;
    }
}

public struct PriorityQueueItem<TKey> {
    public readonly TKey key;
    public float priority;

    public PriorityQueueItem(TKey key, float priority) {
        this.key = key;
        this.priority = priority;
    }
}