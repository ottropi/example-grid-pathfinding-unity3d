﻿using System;
using System.Collections.Generic;
using UnityEngine;

/*
 * Heavily inspired by https://www.redblobgames.com/grids/hexagons/
 * Uses axial coordinates with pointy top hexagon.
 */
public class HexGrid : MonoBehaviour {

    public static HexGrid instance;
    public static float hexOuterRadius = 0.5715f;
    public static float hexInnerRadius = (Mathf.Sqrt(3) / 2) * hexOuterRadius;
    public static int gridX = 24;
    public static int gridY = 18;
    public static Vector2 gridBottomLeftCorner =
        new Vector2(-(gridX * hexInnerRadius), -(gridY * hexOuterRadius * 3f) / 4);

    private static float hexOuterRadius3 = 3f * hexOuterRadius;
    private static float sqrt3 = Mathf.Sqrt(3f);

    // Directions in grid
    public enum GD {
        LEFT,
        RIGHT,
        UP_LEFT,
        UP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }

    // Neighbours directions
    // These are axial coordinates with Y axis pointing down
    public static Dictionary<GD, Vector2> neighbourDirections
        = new Dictionary<GD, Vector2>
    {
        {GD.LEFT,         new Vector2 (-1, 0) },
        {GD.RIGHT,        new Vector2 ( 1, 0) },
        {GD.UP_LEFT,      new Vector2 (-1, 1) },
        {GD.UP_RIGHT,     new Vector2 ( 0, 1) },
        {GD.BOTTOM_LEFT,  new Vector2 ( 0,-1) },
        {GD.BOTTOM_RIGHT, new Vector2 ( 1,-1) }
    };

    private Hex[][] grid;

    // Re-usable priority queue for integration field calculations
    private PriorityQueue<Hex> FlowFieldQueue;
    private Hex targetHex;

    public void Awake() {
        instance = this;

        // Creates the grid
        grid = new Hex[gridX][];

        for (int i = 0; i < gridX; i++) {
            grid[i] = new Hex[gridY];
            for (int j = 0; j < gridY; j++) {
                // Lines are translated to the left to make a rectangle
                grid[i][j] = new Hex(this, i - Mathf.FloorToInt((float)j / 2), j);
            }
        }

        // Sets the neighbors
        Vector2 direction;
        for (int i = 0; i < gridX; i++) {
            for (int j = 0; j < gridY; j++) {
                foreach (GD d in Enum.GetValues(typeof(GD))) {
                    if (neighbourDirections.TryGetValue(d, out direction)) {
                        int q = grid[i][j].q + (int)direction.x;
                        int r = grid[i][j].r + (int)direction.y;

                        // Computes the neighbour position in grid[i][j]
                        // j = r
                        // i = q + Mathf.FloorToInt((float)r / 2)
                        // we re-use q and r as i and j
                        q += Mathf.FloorToInt((float)r / 2);

                        if (!(q == i && r == j)
                            &&
                            q >= 0 && r >= 0
                            &&
                            q < gridX && r < gridY) {
                            grid[i][j].AddNeighbor(d, grid[q][r]);
                        }
                    }
                }
            }
        }

        targetHex = grid[gridX/2][gridY/2];
        FlowFieldQueue = new PriorityQueue<Hex>();
        ComputeFlowField();
    }

    public void ToggleWallOnHex(Vector2 p) {
        Hex hex = PixelToHex(p);
        if (hex != null) {
            hex.ToggleWall();
        }
        ComputeFlowField();
    }

    public void ComputeFlowField() {
        
        FlowFieldQueue.Clear();

        // Resets the integration field values to the max value
        for (int i = 0; i < gridX; i++) {
            for (int j = 0; j < gridY; j++) {
                grid[i][j].fieldValue = int.MaxValue;
                grid[i][j].nextInPath = null;
            }
        }

        // Starts from the target
        targetHex.fieldValue = 0;
        FlowFieldQueue.Enqueue(targetHex, targetHex.fieldValue);

        // The 'field value' is the cost to reach the target and is 
        // equal to the sum of the costs to pass through all the hexes in
        // the path.
        // At every iteration:
        // - picks the hex 'currentHex' with the lowest field value, from
        // the PriorityQueue.
        // - for all the neighbors, calculates the field value if the
        // path passed through the 'currentHex'.
        // - If the cost is lower than their current best, updates the
        // path of the neighbors with this new one.
        // - the neighbor is re-added to the queue so that it is iterated
        // over later and updates its own neighbors with the new path.
        Hex current;
        int newNeighborFieldValue;
        while ((current = FlowFieldQueue.Dequeue()) != null) {
            // Iterates through the neighbours
            foreach (KeyValuePair<GD, Hex> KV in current.neighbors) {
                // New field value of the neighbor
                newNeighborFieldValue = current.fieldValue + KV.Value.GetCost();
                if (newNeighborFieldValue < KV.Value.fieldValue) {
                    // New best path found
                    KV.Value.fieldValue = newNeighborFieldValue;
                    KV.Value.nextInPath = current;
                    // Adds neighbor to the queue
                    if (!FlowFieldQueue.Contains(KV.Value)) {
                        FlowFieldQueue.Enqueue(KV.Value, KV.Value.fieldValue);
                    }
                }
            }
        }
    }

    //// Draws the grid
    //public void OnDrawGizmos() {
    //    UnityEditor.Handles.color = Color.green;
    //    for (int i = 0; i < gridX; i++) {
    //        for (int j = 0; j < gridY; j++) {
    //            UnityEditor.Handles.DrawPolyLine(grid[i][j].corners);
    //            //UnityEditor.Handles.Label(grid[i][j].pos, new GUIContent(
    //            //    grid[i][j].q.ToString() + "," + grid[i][j].r.ToString()));
    //        }
    //    }
    //}

    public Vector2 HexToPixel(Hex hex) {
        float x = hexOuterRadius * sqrt3 * (hex.q + (float)hex.r / 2);
        float y = hexOuterRadius * (3f / 2f) * hex.r;

        // Lower right corner
        x += gridBottomLeftCorner.x;
        y += gridBottomLeftCorner.y;

        return new Vector2(x, y);
    }

    public Hex PixelToHex(Vector2 p) {
        Hex res = null;
        int rq, rr;//rounded

        p -= new Vector2(gridBottomLeftCorner.x, gridBottomLeftCorner.y);

        float q = (p.x * sqrt3 - p.y) / hexOuterRadius3;
        float r = (p.y * 2f) / hexOuterRadius3;

        HexRound(q, r, out rq, out rr);

        //Compensation for the lines translations (see grid creation)
        rq += rr / 2;

        if (rq < gridX && rr < gridY && rq >= 0 && rr >= 0) {
            res = grid[rq][rr];
        }

        return res;
    }

    public static void HexRound(float q, float r, out int rq, out int rr) {
        float x, y, z;
        int rx, ry, rz;//rounded
        float dx, dy, dz;
        AxialToCube(q, r, out x, out y, out z);

        rx = (int)Mathf.Round(x);
        ry = (int)Mathf.Round(y);
        rz = (int)Mathf.Round(z);

        dx = Mathf.Abs(rx - x);
        dy = Mathf.Abs(ry - y);
        dz = Mathf.Abs(rz - z);

        if (dx > dy && dx > dz) {
            rx = -ry - rz;
        } else if (dy > dz) {
            ry = -rx - rz;
        } else {
            rz = -rx - ry;
        }

        CubeToAxial(rx, ry, rz, out rq, out rr);
    }

    public static void AxialToCube(float q, float r,
        out float x, out float y, out float z) {
        x = q;
        z = r;
        y = -x - z;
    }

    public static void AxialToCube(int q, int r,
    out int x, out int y, out int z) {
        x = q;
        z = r;
        y = -x - z;
    }

    public static void CubeToAxial(int x, int y, int z, out int q, out int r) {
        q = x;
        r = z;
    }
}