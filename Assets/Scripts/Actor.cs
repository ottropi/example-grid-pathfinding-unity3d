﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour {

    private static float speed = 10f;

    public void Start() {
        // Verifies if inside the grid bounds
        Hex startHex = HexGrid.instance.PixelToHex(transform.position);
        if (startHex == null) {
            Destroy(gameObject);
        } else {
            StartCoroutine(MoveProcess(startHex));
        }
    }

    // Follows the path to the target Hex calculated by the flow field.
    private IEnumerator MoveProcess(Hex startHex) {

        float t, totalDistance;
        Vector2 startPos, endPos;

        Hex endHex = startHex.nextInPath;

        // The path could change during the movement, so always check
        // for null values
        while (endHex != null) {

            startPos = startHex.pos;
            endPos = endHex.pos;

            totalDistance = Vector2.Distance(startPos, endPos);
            t = 0;

            while (t / totalDistance < 1) {
                transform.position = Vector2.Lerp(startPos, endPos, t / totalDistance);
                t += Time.deltaTime * speed;
                yield return null;
            }

            startHex = endHex;
            endHex = endHex.nextInPath;
        }

        Destroy(gameObject);
    }
}
